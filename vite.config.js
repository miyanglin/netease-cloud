import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [vue()],
  // server: {
  //   // port: 3000,
  //   proxy: {
  //     '/testaxios': {
  //       target: 'https://api.inews.qq.com/',
  //       // target就是你要访问的目标地址，可以是基础地址，这样方便在这个网站的其他api口调用数据
  //       ws: true,
  //       changeOrigin: true,
  //       rewrite: (path) => path.replace(/^\/testaxios/, ''),
  //       // 要记得加rewrite这句
  //     },
  //   },
  // },
})
