import $http from "../utils/request";

// 二维码登录

export const qrcodeAPI = (timestamp) => {
  return $http({
    method:'GET',
    url:'login/qr/key',
    params:{
      timestamp
    }
  })
}


export const createAPI = (key,qrimg) => {
  return $http({
    method:'GET',
    url:'login/qr/create',
    params:{
      key,
      qrimg
    }
  })
}

export const testingAPI = (key,timestamp) => {
  return $http({
    method:'GET',
    url:'login/qr/check',
    params:{
      key,
      timestamp
    }
  })
}


export const statusAPI = () => {
  return $http({
    method:'GET',
    url:'login/status'
  })
}

export const detailAPI = (uid ) => {
  return $http({
    method:'GET',
    url:'user/detail',
    params:{
      uid 
    }
  })
}
//发送验证码
export const captchalAPI = (phone) => {
  return $http({
    method:'GET',
    url:'captcha/sent',
    params:{
      phone 
    }
  })
}

// 验证验证码
export const captchaAPI = (phone,captcha) => {
  return $http({
    method:'GET',
    url:'captcha/verify',
    params:{
      phone,
      captcha 
    }
  })
}

// 验证验证码
export const cellphoneAPI = (phone,captcha) => {
  return $http({
    method:'GET',
    url:'login/cellphone',
    params:{
      phone,
      captcha 
    }
  })
}

// 获取用户详情
export const userAPI = (uid) => {
  return $http({
    method:'GET',
    url:'user/detail',
    params:{
      uid 
    }
  })
}

// banner
export const bannerAPI = () => {
  return $http({
    method:'GET',
    url:'banner'
  })
}

// 热门歌单分类
export const hotAPI = () => {
  return $http({
    method:'GET',
    url:'playlist/hot'
  })
}

// 歌单
export const playlistAPI = (limit) => {
  return $http({
    method:'GET',
    url:'top/playlist',
    params:{
      limit
    }
  })
}

// 获取歌单详情
export const detailsAPI = (id) => {
  return $http({
    method:'GET',
    url:'playlist/detail',
    params:{
      id
    }
  })
}

// 获取歌单所有歌曲
export const playlistsAPI = (id) => {
  return $http({
    method:'GET',
    url:`playlist/track/all?id=${id}&limit=20&offset=1`,
  })
}