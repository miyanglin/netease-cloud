import {createRouter,createWebHashHistory} from 'vue-router'
import Hpage from '../views/Home/Hpage/hpage.vue'
import Home from '../views/Home/home.vue'
let routes = [
  {
    path:'/',component:Home,redirect:'/hpage',children:[
    {
      path:'/hpage',
      component:Hpage,
      meta:{ name:'发现音乐',id:1},
      redirect:'/hpage/recommend',
      children:[
        {
          path:'/hpage/recommend',
          component:() => import('../views/Home/views/Recommend/recommend.vue'),
          meta:{ name:'推荐' ,id: 1 ,isShow: true}
        },
        {
          path:'/hpage/rankinglist',
          component:() => import('../views/Home/views/Rankinglist/rankinglist.vue'),
          meta:{ name:'排行耪' ,id: 2 ,isShow: true}
        },
        {
          path:'/hpage/songsheet',
          component:() => import('../views/Home/views/Songsheet/songsheet.vue'),
          meta:{ name:'歌单' ,id: 3 ,isShow: true }
        },
        {
          path:'/hpage/anchor',
          component:() => import('../views/Home/views/Anchor/anchor.vue'),
          meta:{ name:'主播电台' ,id: 4 ,isShow: true}
        },
        {
          path:'/hpage/singer',
          component:() => import('../views/Home/views/Singer/singer.vue'),
          meta:{ name:'歌手' ,id: 5 ,isShow: true}
        },
        {
          path:'/hpage/newdisc',
          component:() => import('../views/Home/views/Newdisc/newdisc.vue'),
          meta:{ name:'新碟上价' ,id: 6,isShow: true}
        },
        {
          path:'/hpage/details/:id',
          component:() => import('../views/Home/views/Recommend/details.vue'),
          meta:{ isShow: false,id: 7 } 
        },
      ]
    },
    {
      path:'/mymusic',
      component:()=> import('../views/Mymusic/mymusic.vue'),
      meta:{ name:'我的音乐' ,id:2}
    },
    {
      path:'/follow',
      component:()=> import('../views/Follow/follow.vue'),
      meta:{ name:'关注',id:23 }
    },
    {
      path:'/shopping',
      component:()=> import('../views/Shopping/shopping.vue'),
      meta:{ name:'商城',id:4 }
    },
    {
      path:'/terminal',
      component:()=> import('../views/Terminal/terminal.vue'),
      meta:{ name:'下载客服端',id:5 }
    },
  ]}
 
]
const router = createRouter({
  history:createWebHashHistory(),
  routes
})
// router.beforeEach((to,from,next)=> {
//   next()
// })
export default router