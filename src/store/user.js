import {defineStore} from 'pinia'
import {userAPI} from '../api'
const useData = defineStore('data',{
  state:()=>{
    return{
      userdata:[]
    }
  },
  actions:{
    async setUser(){
     console.log(this.userdata.userId);
     const res = userAPI(this.userdata.userId)
     console.log(res);
    }
  },
  persist:{
    enabled:true,
    strategies:[
     {
      key:'userdata',
      storage:sessionStorage
     }
    ]
  }
})

export default useData
