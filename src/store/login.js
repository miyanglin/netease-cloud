import {defineStore} from 'pinia'

const useLogin = defineStore('login',{
  state:()=>{
    return{
      cookie:''
    }
  },
  actions:{
    setCookie(){
    }
  },
  persist:{
    enabled:true,
    strategies:[
     {
      key:'cookie',
      storage:sessionStorage
     }
    ]
  }
})

export default useLogin
