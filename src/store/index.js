import useLogin from './login'
import useData from './user'
export default function useStore(){
  return {
    useLogin:useLogin(),
    useData:useData(),
  }
}