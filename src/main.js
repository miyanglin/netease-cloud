import { createApp } from 'vue'
import { createPinia } from 'pinia';
import piniaPluginPersistedstate  from 'pinia-plugin-persistedstate'
import './style.css'
import App from './App.vue'
import router from './router'
// 引入安装包和样式

const pinia = createPinia();
pinia.use(piniaPluginPersistedstate)
import Antd from 'ant-design-vue';
import 'ant-design-vue/dist/antd.css';

createApp(App).use(router).use(pinia).use(Antd).mount('#app')
